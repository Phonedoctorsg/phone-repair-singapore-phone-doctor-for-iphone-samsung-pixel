PHONEDOCTOR+ provides our customers with the REAL 3 Months Warranty for our repair parts. There are many others who pretend to provide longer warranty periods on paper but only provide excuses or disappear when you try to claim it. We only ask they you return the product unbroken and with your valid receipt.
We are really good at what we do. PhoneDoctor+ has a strong edge over other repairs shops as we have the strength in numbers.If you need your repairs done URGENT & within a specific time, please make it known to our staff and we will try our best to expedite your repair.

Address: 311 New Upper Changi Rd, B1-14, Singapore 467360

Phone: +65 9777 7509

Website: https://phonedoctor.com.sg
